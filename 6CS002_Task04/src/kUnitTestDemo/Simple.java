package kUnitTestDemo;

public class Simple {

  public int input_1 = 22;
  private int input_2 = 41;
  
  public Simple() {
  }
  
  public Simple(int input_1, int input_2) {
    this.input_1 = input_1;
    this.input_2 = input_2;
  }

  public void squareinput_1(){
    this.input_1 *= this.input_1;
  }

  private void squareinput_2(){
    this.input_2 *= this.input_2;
  }

  public int getinput_1() {
    return input_1;
  }

  private void setinput_1(int input_1) {
    this.input_1 = input_1;
  }

  public int getinput_2() {
    return input_2;
  }

  public void setinput_2(int input_2) {
    this.input_2 = input_2;
  }
  
  public String toString() {
    return String.format("(input_1:%d, input_2:%d)", input_1, input_2);
  }

}
