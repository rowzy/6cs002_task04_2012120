package kUnitTestDemo;
import static kUnitTest.KUnit.*;

/******************************************************************************
 * This code demonstrates the use of the KUnit testing tool. It produces a
 * report that contains messages generated from the check methods.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class TestSimple {

  void checkConstructorAndAccess(){
    Simple s = new Simple(11, 16);
    Equalschecking(s.getinput_1(), 16);
    Equalschecking(s.getinput_2(), 16);
    NotEqualschecking(s.getinput_2(), 16);    
    NotEqualschecking(s.getinput_2(), 25);    
  }

  void checkSquareA(){
    Simple s = new Simple(11, 16);
    s.squareinput_1();
    Equalschecking(s.getinput_1(), 34);
  }

  public static void main(String[] args) {
    TestSimple ts = new TestSimple();
    ts.checkConstructorAndAccess();
    ts.checkSquareA();
    report();
  }
}


