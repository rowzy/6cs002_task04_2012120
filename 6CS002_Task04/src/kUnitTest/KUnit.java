package kUnitTest;

import java.util.*;

/******************************************************************************
 * This code is a prototype of a unit testing system. It is very primitive and 
 * contains only a reporting system and a means of checking assertions.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class KUnit {
  private static List<String> Listchecks;
  private static int checksMade = 0;
  private static int pass_Checks = 0;
  private static int fail_Checks = 0;

  /******************************************************************************
   * A report is formed from a list of strings. This method adds a message to the
   * report. 
   * 
   * @author Dr Kevan Buckley, University of Wolverhampton, 2019
   ******************************************************************************/

  private static void Report(String txt) {
    if (Listchecks == null) {
    	Listchecks = new LinkedList<String>();
    }
    Listchecks.add(String.format(" %04d: %s", checksMade++, txt));
  }

  /******************************************************************************
   * This method is similar to an assertion in JUnit. It checks that two integers
   * are equal and adds an appropriate message to the report.
   * 
   * @author Dr Kevan Buckley, University of Wolverhampton, 2019
   ******************************************************************************/
  
  public static void Equalschecking(int value_1, int value_2) {
    if (value_1 == value_2) {
    	Report(String.format("  %d == %d", value_1, value_2));
      pass_Checks++;
    } else {
    	Report(String.format("* %d == %d", value_1, value_2));
      fail_Checks++;
    }
  }

  /******************************************************************************
   * This method is similar to an assertion in JUnit. It checks that two integers
   * are not equal and adds an appropriate message to the report.
   * 
   * @author Dr Kevan Buckley, University of Wolverhampton, 2019
   ******************************************************************************/

  public static void NotEqualschecking(int value_1, int value_2) {
    if (value_1 != value_2) {
    	Report(String.format("  %d != %d", value_1, value_2));
      pass_Checks++;
    } else {
    	Report(String.format("* %d != %d", value_1, value_2));
      fail_Checks++;
    }
  }

  /******************************************************************************
   * Outputs the messages that form the report.
   * 
   * @author Dr Kevan Buckley, University of Wolverhampton, 2019
   ******************************************************************************/

  public static void report() {
    System.out.printf("%d checks passed\n", pass_Checks);
    System.out.printf("%d checks failed\n", fail_Checks);
    System.out.println();
    
    for (String check : Listchecks) {
      System.out.println(check);
    }
  }
}
