package reflection;
import java.lang.reflect.Field;

public class SeventhReflection {
	public static void main(String[] args) throws Exception {
		SimpleRef obj = new SimpleRef();
		System.out.println(" SeventhReflection");
		Field [] fields = obj.getClass().getDeclaredFields();
		System.out.printf(" There are %d fields\n", fields.length);
		for(Field f : fields) {
		f.setAccessible(true);
		System.out.printf(" field name:%s\n type:%s\n value:%.2f\n", f.getName(), 
		f.getType(), f.getDouble(obj));
		}
		}
}



