package reflection;
import java.lang.reflect.Method;

public class NinthReflection {
	public static void main(String[] args) throws Exception {
		SimpleRef obj = new SimpleRef();
		System.out.println(" NinthReflection");
		Method [] methods = obj.getClass().getMethods();
		System.out.printf(" There are %d methods\n", methods.length);
		for(Method m : methods) {
		System.out.printf("   method name:%s\n   type:%s\n   parameters:", m.getName(), 
		m.getReturnType());
		Class [] types = m.getParameterTypes();
		for(Class c : types) {
		System.out.print(c.getName() + " " );
		}
		System.out.println();
		}
		}

}
