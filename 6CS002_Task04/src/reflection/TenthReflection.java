package reflection;
import java.lang.reflect.Method;

public class TenthReflection {
	public static void main(String[] args) throws Exception {
		SimpleRef obj = new SimpleRef();
		System.out.println(" TenthReflection");
		Method m = obj.getClass().getDeclaredMethod("setInput_2", double.class);
		m.setAccessible(true);
		m.invoke(obj, 100);
		System.out.println(obj);
		}
}


