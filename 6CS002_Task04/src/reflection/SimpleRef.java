package reflection;

public class SimpleRef {
	public double firstinput = 72.5;
	private double secondinput = 12.3;
	public SimpleRef() {
	}
	public SimpleRef(double input_1, double input_2) {
	this.firstinput = input_1;
	this.secondinput = input_2;
	}
	public void sqrtInput_1() {
	this.firstinput = Math.sqrt(this.firstinput);
	}
	private void sqrtInput_2() {
	this.secondinput = Math.sqrt(this.secondinput);
	}
	public double getInput_1() {
	return firstinput;
	}
	private void setInput_1(double input_1) {
	this.firstinput = input_1;
	}
	public double getInput2() {
	return secondinput;
	}
	public void setInput_2(double input_2) {
	this.secondinput = input_2;
	}
	public String toString() {
	return String.format("Input_1 : %.2f\nInput_2 : %.2f", firstinput, secondinput);
	}
	}


