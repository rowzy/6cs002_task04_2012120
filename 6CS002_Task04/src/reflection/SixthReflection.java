package reflection;
import java.lang.reflect.Field;

public class SixthReflection {
	public static void main(String[] args) throws Exception {
		SimpleRef obj = new SimpleRef();
		System.out.println("SixthReflection");
		Field [] fields = obj.getClass().getDeclaredFields();
		System.out.printf("There are %d fields\n", fields.length);
		for(Field f : fields) {
		System.out.printf("field name:%s\n type:%s\n accessible:%s\n", f.getName(), 
		f.getType(), f.isAccessible());
		}
		}

}

